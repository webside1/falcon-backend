const User = require('../models/user')

exports.allUsers = async (req, res) => {
 User.find((err, users) => { 
   if (err | !users) {
     return res.status(400).json({
       error: err
     })
   }
   res.json(users)
 }).select('name')
};
