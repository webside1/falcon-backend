const jwt = require('jsonwebtoken')
require('dotenv').config()
const User = require('../models/user')

exports.signup = async (req, res) => {
  const userExists = await User.findOne({email: req.body.email})
  if(userExists) return res.status(403).json({
    error: "Adres e-mail jest zajęty!"
  })
  const user = await new User(req.body)
  await user.save()
  res.status(200).json({ message: "Rejestracja udana! Zaloguj się." })
};

exports.signin = (req, res) => {
  // find the user based on email
  const {email, password} = req.body;
  User.findOne({email}, (err, user) => {
    // if err or no user
    if (err || !user) {
      return res.status(401).json({
        error: "Użytkownik z tym adresem e-mail nie istnieje. Proszę się zarejestrować."
      })
    }
    // if user is found make sure the email and password match
    // create authenticate method in model and use here
    if (!user.authenticate(password)) {
      return res.status(401).json({
        error: "Adres e-mail lub hasło nie są zgodne"
      });
    }
    // generate a token with user id and secret
    const token = jwt.sign({ _id: user.id }, process.env.JWT_SECRET)
    // persist the token as 't' in cookie with expiry date
    res.cookie("t", token, {expire: new Date() + 9999})
    // return response with user and token to frontend
    const {_id, name, email} = user
    return res.json({token, user: {_id, email, name}})
  });
};

exports.signout = (req, res) => {
  res.clearCookie("t");
  return res.json({message: "Wylogowano pomyślnie!"});
};