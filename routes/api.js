const express = require('express')
const router = express.Router()
const User = require('../models/user')
const { findOne } = require('../models/user')

// router.get('/', (req, res) => {
//   res.send('from Ari route')
// })

router.post('/register', (req, res) => {
  let userData = req.body
  let user = new User(userData)
  user.save((error, registeredUser) => {
    if (error) {
      console.loge('Nooo Error', error)
    } else {
      res.status(200).send(registeredUser)
    }
  })
})

router.post('/login', (req, res) => {
  let userData = req.body

  User.findOne({email: userData.email}, (error, user) => {
    if (error) {
      console.log('jest', error)
    } else {
      if (!user) {
        res.status(401).send('Invalid email')
      } else if ( user.password !== userData.password) {
        res.status(401).send('Invalid password')
      } else {
        res.status(200).send(user)
      }
    }
  })
})

module.exports = router
