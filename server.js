const express = require('express')
const app = express()
const mongoose = require('mongoose')
const fs = require('fs')
const morgan = require('morgan')
const cookieParser = require('cookie-parser')
const expressValidator = require("express-validator")
const cors = require("cors");
const dotenv = require('dotenv')
dotenv.config()

// Connect to database
mongoose
  .connect(
    process.env.MONGO_URI,
    {
      useNewUrlParser: true,
      useUnifiedTopology: true
    },
  )
  .then(() => console.log('DB Connected'))

mongoose.connection.on('error', err => {
  console.log(`DB connection error: ${err.message}`)
});

// bring in routes
const authRoutes= require("./routes/auth")
const userRoutes= require("./routes/user")
const api = require('./routes/api')

app.get("/api", (req, res) => {
  fs.readFile('docs/apiDocs.json', (err, data) => {
      if (err) {
          res.status(400).json({
              error: err
          });
      }
      const docs = JSON.parse(data);
      res.json(docs);
  });
});

// middleware
app.use(morgan('dev'))
app.use(express.urlencoded({ extended: true }))
app.use(express.json())
app.use(cookieParser())
app.use(expressValidator())
app.use(cors())
app.use('/api', authRoutes)
app.use('/api', userRoutes)
app.use('/api', api)

const port = process.env.PORT || 3000
app.listen(port, ()=> {
  console.log(`A Node Js API is listening on port: ${port}`)
})